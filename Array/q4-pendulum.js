let readline = require("readline-sync");

numberOfArrays = readline.question("Number of arrays");

for (i = 0; i < numberOfArrays; i++) {
  let length = readline.question("Enter array length");

  let result = [];

  let givenArray = readline.question("Enter the array").split(" ");
  givenArray.sort((a, b) => a - b);

  let count = 0;
  result.push(givenArray[0]);

  for (j = 1; j < length; j++) {
    count++;
    if (count % 2 != 0) {
      result.push(givenArray[j]);
    } else {
      result.unshift(givenArray[j]);
    }
  }

  console.log(result);
}
