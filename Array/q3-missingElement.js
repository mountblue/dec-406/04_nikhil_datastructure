// Given an increasing sequence a[], we need to find the K-th missing contiguous element in the increasing sequence which is not present in the sequence. If no k-th missing element is there output -1.

let readline = require("readline-sync");

numberOfArrays = readline.question("Number of arrays");

for (i = 0; i < numberOfArrays; i++) {
  let inputDetails = readline
    .question("Enter array length and kth number")
    .split(" ");
  let givenArray = readline.question("Enter the array").split(" ");
  let initialNum = givenArray[0];
  let count = 0;
  let flag = false;

  for (j = 0; j < givenArray.length; j++) {
    if (!(givenArray[j] == initialNum)) {
      count++;

      if (count == inputDetails[1]) {
        console.log(initialNum);
        flag = true;
        break;
      } else {
        initialNum++;
      }
    }
    initialNum++;
  }
  if (!flag) {
    console.log(-1);
  }
}
