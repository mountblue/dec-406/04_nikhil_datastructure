//1. Given an array A of size N, construct a Sum Array S(of same size) such that S is equal to the sum of all the elements of A except A[i]. Your task is to complete the function SumArray(A, N) which accepts the array A and N(size of array).

let readline = require("readline-sync");

let numOfInputs = parseInt(readline.question("Enter number of inputs"));

for (i = 0; i < numOfInputs; i++) {
  let result = [];

  let givenArray = readline.question("Enter the array");
  let numArray = givenArray.split(" ");
  let sumOfArray = numArray.reduce((a, b) => parseInt(a) + parseInt(b), 0);

  for (j = 0; j < numArray.length; j++) {
    result.push(sumOfArray - parseInt(numArray[j]));
  }
  console.log(result);
}
