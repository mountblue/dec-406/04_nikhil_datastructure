//pop nth node

let LinkedList = require("./linkedlist.js");

let givenArray = "5 2 3 4".split(" ");

let ls = new LinkedList();
for (let i = 0; i < givenArray.length; i++) {
  ls.add(givenArray[i]);
}
console.log(`Before pop: ${ls.printElement()}`);
console.log(`popped value: ${ls.pop(4)}`);
console.log(`After Pop: ${ls.printElement()}`);
