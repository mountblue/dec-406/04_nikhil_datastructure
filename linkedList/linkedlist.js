//Lisnked list Data Structure
// add(data):null
//        add the node to the linked list
//              arguments:
//                    data: node value
//              return:
//                    null
// insertAt(data,index):null
//        insert the data/node at a given index
//              arguments:
//                    data: node value
//                    index (int): position at which data needs to be inserted
//              return:
//                    null
// printElement():array
//        print the data of all the nodes
//              arguments:
//              return:
//                    array of the data of each node
// dataAt(index): data
//        get the data at the given node/index
//              arguments:
//                    index (int): position of the node
//              return:
//                    data present at the node
// count(data):int
//        return the number of times data repeats
//              argument:
//                    data: node value that needs to be checked of repetation count
//              return:
//                    number of times the data repeated
// pop(index=0):data
//        remove and return the node from the linked list from the given position
//              argument:
//                    index (int): position of the node | default position is 0
//              return:
//                    data of the node removed from the list
// sortInsert(data):null
//        insert the data while maintaining the sorting order
//              argument:
//                    data: node/value that needs to be inserted
//              return:
//                  null
// removeDuplicate():null
//        removes the duplicate values from the linked lisr
//              argument:
//              return:
//                  null

class Node {
  constructor(data) {
    this.data = data;
    this.next = null;
  }
}

class LinkedList {
  constructor() {
    this.head = null;
    this.size = 0;
  }

  add(data) {
    let node = new Node(data);

    if (this.head) {
      let currentNode = this.head;

      while (currentNode.next) {
        currentNode = currentNode.next;
      }
      currentNode.next = node;
    } else {
      this.head = node;
    }
    this.size += 1;
    return null;
  }

  insertAt(data, index) {
    let i = 0;
    if (index > this.size) {
      console.log("index out of bound");
      return null;
    }
    let node = new Node(data);

    if (index == 0) {
      node.next = this.head;
      this.head = node;
    } else {
      let presentNode = this.head;
      while (presentNode.next) {
        i++;

        if (index == i) {
          node.next = presentNode.next;
          presentNode.next = node;
        }
        presentNode = presentNode.next;
      }
    }
  }

  printElement() {
    if (this.size == 0) {
      return "empty list";
    } else if (this.size == 1) {
      return this.head.data;
    } else {
      let presentNode = this.head;
      let arr = [];
      while (presentNode) {
        arr.push(presentNode.data);
        presentNode = presentNode.next;
      }
      return arr;
    }
  }

  dataAt(index) {
    let i = 0;
    if (index >= this.size || index < 0) {
      console.log("index out of bound");
    } else if (index == 0) {
      // console.log(this.head.data);
      return this.head.data;
    } else {
      let presentNode = this.head;
      while (presentNode.next) {
        presentNode = presentNode.next;
        i++;
        if (index == i) {
          // console.log(presentNode.data);
          return presentNode.data;
        }
      }
      console.log(presentNode.next.data);
      return presentNode.next.data;
    }
  }

  count(value) {
    let presentNode = this.head;
    let count = 0;

    while (presentNode) {
      if (presentNode.data == value) {
        count++;
      }
      presentNode = presentNode.next;
    }
    return count;
  }

  pop(index = 0) {
    if (index < 0 || index >= this.size) {
      return "index out of bound";
    }
    let presentNode = this.head;
    let i = 0;
    let prevnode = presentNode;
    while (presentNode) {
      if (index == 0) {
        this.head = presentNode.next;
        let temp = presentNode.data;
        presentNode.next = null;
        presentNode = null;
        this.size -= 1;
        return temp;
      } else if (index == i) {
        prevnode.next = presentNode.next;
        let temp = presentNode.data;
        presentNode.next = null;
        presentNode = null;
        this.size -= 1;
        return temp;
      }
      prevnode = presentNode;
      presentNode = presentNode.next;
      i++;
    }
  }

  sortInsert(value) {
    let presentNode = this.head;
    let node = new Node(value);
    let flag = false;

    if (presentNode.data >= value) {
      node.next = presentNode;
      this.head = node;
    } else {
      let prevnode = presentNode;
      while (presentNode) {
        if (value <= presentNode.data) {
          prevnode.next = node;
          node.next = presentNode;
          break;
        } else if (!presentNode.next) {
          presentNode.next = node;
          break;
        } else {
          prevnode = presentNode;
          presentNode = presentNode.next;
        }
      }
    }
    return null;
  }

  removeDuplicate() {
    let checkDict = {};

    let presentNode = this.head;
    let prevnode = presentNode;

    while (presentNode) {
      if (checkDict[presentNode.data]) {
        prevnode.next = presentNode.next;
      } else {
        checkDict[presentNode.data] = 1;
        prevnode = presentNode;
      }

      presentNode = presentNode.next;
    }
    return null;
  }
}

module.exports = LinkedList;
