//segregate even and odd preserving the order

let LinkedList = require("./linkedlist.js");

let givenArray = "17 15 8 9 2 4 6".split(" ");

let ls = new LinkedList();
for (let i = 0; i < givenArray.length; i++) {
  ls.add(givenArray[i]);
}

// ls.printElement();

let result = new LinkedList();
let count = -1;

for (let i = 0; i < ls.size; i++) {
  if (ls.dataAt(i) % 2 == 0) {
    count++;
    result.insertAt(ls.dataAt(i), count);
  } else {
    result.add(ls.dataAt(i));
  }
}

console.log(`result: ${result.printElement()}`);
