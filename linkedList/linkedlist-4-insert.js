//insert value in sorted list

let LinkedList = require("./linkedlist.js");

let givenArray = "1 2 3 5".split(" ");

let ls = new LinkedList();
for (let i = 0; i < givenArray.length; i++) {
  ls.add(givenArray[i]);
}

console.log(`Before inserting: ${ls.printElement()}`);

ls.sortInsert(5);

console.log(`After inserting: ${ls.printElement()}`);
