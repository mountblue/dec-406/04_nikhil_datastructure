//get count of value

let LinkedList = require("./linkedlist.js");

let givenArray = "5 1 3 3 3 3 5".split(" ");

let ls = new LinkedList();
for (let i = 0; i < givenArray.length; i++) {
  ls.add(givenArray[i]);
}

console.log(ls.count(5));
