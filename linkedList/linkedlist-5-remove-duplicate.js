let LinkedList = require("./linkedlist.js");

let givenArray = "1 2 7 7 2 2 7 3 4 4 3 5 6".split(" ");

let ls = new LinkedList();
for (let i = 0; i < givenArray.length; i++) {
  ls.add(givenArray[i]);
}

ls.removeDuplicate();
console.log(ls.printElement());
