let givenArray = "1 2 3 4 5 6 7 8 9".split(" ");
let ddimension = 3;
let rdimension = 3;

let matrix = [];
let arr;
let n = -1;
for (let i = 0; i < givenArray.length; i++) {
  if (i % rdimension == 0) {
    arr = [];
    n += 1;
  }
  arr.push(givenArray[i]);
  matrix[n] = arr;
}

let result = [];

for (let i = ddimension - 1; i >= 0; i--) {
  for (let j = 0; j < rdimension; j++) {
    result.push(matrix[j][i]);
  }
}
console.log(result);
