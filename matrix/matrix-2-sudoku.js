let inputMatrix = "3 0 6 5 0 8 4 0 0 5 2 0 0 0 0 0 0 0 0 8 7 0 0 0 0 3 1 0 0 3 0 1 0 0 8 0 9 0 0 8 6 3 0 0 5 0 5 0 0 9 0 6 0 0 1 3 0 0 0 0 2 5 0 0 0 0 0 0 0 0 7 4 0 0 5 2 0 6 3 0 0".split(
  " "
);

let matrix = [];
let arr;
let n = -1;
for (i = 0; i < inputMatrix.length; i++) {
  if (i % 9 == 0) {
    arr = [];
    n += 1;
  }
  arr.push(inputMatrix[i]);
  matrix[n] = arr;
}

for (j = 0; j < 9; j++) {
  //   console.log("ASd");
  let rowDict = {};
  let columnDict = {};
  for (k = 0; k < 9; k++) {
    if (matrix[j][k] != 0) {
      if (rowDict[matrix[j][k]]) {
        console.log("duplicate row");
        // console.log(`co-or = ${j} ${k}`);
        break;
      } else {
        rowDict[matrix[j][k]] = 1;
      }
    }
    if (matrix[k][j] != 0) {
      if (columnDict[matrix[k][j]]) {
        console.log("duplicate column");
        // console.log(`co-or = ${k} ${j}`);
        break;
      } else {
        columnDict[matrix[k][j]] = 1;
      }
    }
  }
}

for (let x = 0; x < 3; x++) {
  for (let y = 0; y < 3; y++) {
    checkGrid(x * 3, y * 3);
  }
}

function checkGrid(xinit, yinit) {
  let gridDict = {};
  //   console.log(`x:${xinit} y:${yinit}`);
  for (let l = xinit; l < xinit + 3; l++) {
    for (let m = yinit; m < yinit + 3; m++) {
      if (matrix[l][m] != 0) {
        if (gridDict[matrix[l][m]]) {
          console.log("invalid grid");
          break;
        } else {
          gridDict[matrix[l][m]] = 1;
        }
      }
    }
  }
}
