function makeMatrix(givenArray, rdimension, ddimension) {
  let resultArray = [];
  let matrix = [];
  let arr;
  let n = -1;
  for (let i = 0; i < givenArray.length; i++) {
    if (i % rdimension == 0) {
      arr = [];
      n += 1;
    }
    arr.push(givenArray[i]);
    matrix[n] = arr;
  }

  for (let count = 0; count < 2 * ddimension; count++) {
    let spiralResult;

    spiralResult = spiral(matrix, rdimension, ddimension, count);
    matrix = spiralResult[1];
    resultArray.push(...spiralResult[0]);
    if (matrix.length == 0) {
      break;
    }
    matrix = rotateMatrix(matrix, rdimension, ddimension);
  }

  console.log(resultArray);
}

function spiral(matrix, rdimension, ddimension, count) {
  spliceResult = [];
  let arr = [];
  arr.push(...matrix[0]);
  spliceResult.push(arr);
  matrix.splice(0, 1);
  spliceResult.push(matrix);
  return spliceResult;
}

function rotateMatrix(matrix) {
  // console.log(matrix.length);
  rdimension = matrix[0].length;
  ddimension = matrix.length;
  let result = [];
  for (let i = rdimension - 1; i >= 0; i--) {
    for (let j = 0; j < ddimension; j++) {
      result.push(matrix[j][i]);
    }
  }

  let rMatrix = [];
  let arr;
  let n = -1;
  for (let i = 0; i < result.length; i++) {
    if (i % ddimension == 0) {
      arr = [];
      n += 1;
    }
    arr.push(result[i]);
    rMatrix[n] = arr;
  }
  matrix = rMatrix;
  return rMatrix;
}

let givenArray = "1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16".split(" ");
let ddimension = 4;
let rdimension = 4;

makeMatrix(givenArray, rdimension, ddimension);
