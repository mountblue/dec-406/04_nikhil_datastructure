let pathArray = "0 0 1 0 0 3 3 0 0 3 3 0 2 3 0 0".split(" ");
let dimension = 4;

// if (pathArray.length % 3 != 0) {
//   for (i = 0; i < 3 - (pathArray.length % 3); i++) {
//     pathArray.push("0");
//   }
// }
console.log(pathArray.length);

let matrix = [];
let arr;
let n = -1;
for (let i = 0; i < pathArray.length; i++) {
  if (i % dimension == 0) {
    arr = [];
    n += 1;
  }
  arr.push(pathArray[i]);
  matrix[n] = arr;
}

console.log(matrix);

let sourcex;
let sourcey;

for (let i = 0; i < dimension; i++) {
  for (let j = 0; j < dimension; j++) {
    if (matrix[i][j] == 1) {
      sourcex = j;
      sourcey = i;
    }
  }
}
// console.log(sourcex);
// console.log(sourcey);
let flag = false;
checkNeighbor(sourcey, sourcex);

function checkNeighbor(x, y) {
  //   console.log("enter");
  if (x < dimension && x >= 0 && y < dimension && y >= 0) {
    // console.log(x);
    if (matrix[x][y] == 2) {
      console.log(`Found at x:${x} y:${y}`);
      flag = true;
      return;
    } else if (matrix[x][y] == 3 || matrix[x][y] == 1) {
      //   console.log("ads");
      //   console.log(`Current value ${matrix[x][y]}`);
      matrix[x][y] = 0;
      checkNeighbor(x + 1, y);
      checkNeighbor(x - 1, y);
      checkNeighbor(x, y - 1);
      checkNeighbor(x, y + 1);
    } else {
      //   console.log("nope");
    }
  } else {
  }

  if (flag) {
    return;
  }
}

if (flag) {
  console.log("found");
} else {
  console.log("not found");
}
