// function matrix(x, y) {
//   let directionString = "LLRU";
//   let direction = directionString.split("");

//   let matrixArray = [[], []];

//   for (i = 0; i < x; i++) {
//     for (j = 0; j < y; j++) {
//       matrixArray[i][j] = [i, j];
//     }
//   }
//   console.log(matrixArray);

//   for (k=0;k<direction.length;k++){

//   }
// }

function matrix(downMax, rightMax, stringDirect) {
  let maxGrid = [{ x: rightMax, y: downMax }];
  let flag = false;

  for (i = 0; i < downMax; i++) {
    for (j = 0; j < rightMax; j++) {
      let initialPos = [{ x: j, y: i }];
      console.log(`down ${downMax} up ${rightMax}`);
      flag = checkFailTest(initialPos, downMax, rightMax, stringDirect);
      if (flag == true) {
        console.log("You have reached your destination");
        return 1;
      }
    }
  }
  console.log("BOOOOOOM!!!!");
  return -1;
}

function checkFailTest(initialPos, downMax, rightMax, stringDirect) {
  let directionString = stringDirect.split("");
  //   console.log(initialPos);
  for (k = 0; k < directionString.length; k++) {
    let direction = directionString[k];
    // console.log(`direction: ${direction}`);

    let trainx = initialPos[0].x;
    let trainy = initialPos[0].y;
    if (direction == "L") {
      trainx -= 1;
    } else if (direction == "R") {
      trainx += 1;
    } else if (direction == "U") {
      trainy -= 1;
    } else if (direction == "D") {
      trainy += 1;
    }
    initialPos[0].x = trainx;
    initialPos[0].y = trainy;
    // console.log(initialPos);
    if (
      trainx < 0 ||
      trainx > rightMax - 1 ||
      trainy < 0 ||
      trainy > downMax - 1
    ) {
      return false;
    }
  }

  return true;
}

console.log(matrix(4, 3, "RRUUULLDDRURR"));
