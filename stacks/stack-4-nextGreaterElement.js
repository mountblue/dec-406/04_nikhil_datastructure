let Stack = require("./stacks");
let result = [];
let givenArray = [4, 5, 1, 8, 7, 9, 10];
let givenStack = new Stack();
let stack = new Stack();

for (let i = 0; i < givenArray.length; i++) {
  givenStack.push(givenArray[i]);
}
console.log(givenStack.displayStack());

while (givenStack.size) {
  stack.push(givenStack.pop());
}

while (stack.size) {
  let currentNum = stack.pop();

  let tempStact = new Stack();
  tempStact = stack.clone();

  while (tempStact.size) {
    let checkValue = tempStact.pop();

    if (checkValue > currentNum) {
      result.push(checkValue);
      break;
    }
    if (tempStact.size == 0) {
      result.push(-1);
      break;
    }
  }

  if (stack.size == 0) {
    result.push(-1);
  }
}
console.log(result);
