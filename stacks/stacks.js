// Implementation of Stack Data Structure
//
//push(data):null
//          add data at the top of the stack
//              argument:
//                  data: data that needs to be added
//              return:
//                  null
//pop():data
//          remove the data from the top of the stack
//              argument:
//              return:
//                  the data which is removed from the stack
//isEmpty():boolean
//          check if stack is empty
//              argument:
//              return:
//                  true if stack is empty | false if stack is not empty
//displayStack():array
//          display the content of the stack
//              argument:
//              return:
//                  returns the array containing the content of the stack
//clone():Stack
//          create a clone of the stack
//              argument:
//              return:
//                  returns the Stack object

class Stack {
  constructor() {
    this.items = {};
    this.size = 0;
  }

  push(data) {
    this.size += 1;
    this.items[this.size] = data;
  }

  pop() {
    if (this.size != 0) {
      let temp = this.items[this.size];
      delete this.items[this.size];
      this.size -= 1;
      return temp;
    } else {
      return "Stack is empty";
    }
  }

  isEmpty() {
    if (this.size == 0) {
      return true;
    } else {
      return false;
    }
  }

  displayStack() {
    if (this.size != 0) {
      return Object.values(this.items);
    }
    return null;
  }

  clone() {
    if (this.size >= 0) {
      let temp = new Stack();
      let tempSize = 0;
      while (tempSize < this.size) {
        tempSize++;
        temp.push(this.items[tempSize]);
      }
      temp.size = this.size;
      return temp;
    }
  }
}

module.exports = Stack;
