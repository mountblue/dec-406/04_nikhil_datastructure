// Print Bracket Number. Given an expression exp of length n consisting of some brackets.
// The task is to print the bracket numbers when the expression is being parsed.

let Stack = require("./stacks");

let givenArray = "(()((())))".split("");
let result = new Stack();
console.log(givenArray);
let openStack = new Stack();
let open = 0;

for (let i = 0; i < givenArray.length; i++) {
  if (givenArray[i] == "(") {
    open++;
    result.push(open);
    openStack.push(open);
  } else if (givenArray[i] == ")") {
    if (openStack.size != 0) {
      result.push(openStack.pop());
    } else {
      console.log("invalid brackets");
      break;
    }
  }
}

console.log(result.displayStack());
