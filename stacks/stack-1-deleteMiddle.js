//Delete the middle element of the stack. Given a stack with
//push(), pop(), empty() operations, delete middle of it without using any additional data structure.

let Stack = require("./stacks");

let givenArray = [1, 5, 4, 3, 5, 4, 7, 8];
let stack = new Stack();
let tempStack = new Stack();

for (let i = 0; i < givenArray.length; i++) {
  stack.push(givenArray[i]);
}

let stackMid = Math.floor(stack.size / 2) + 1;
while (stack.size > stackMid) {
  tempStack.push(stack.pop());
}

console.log(`middle element is ${stack.pop()}`);

while (tempStack.size > 0) {
  stack.push(tempStack.pop());
}
console.log(stack.displayStack());
