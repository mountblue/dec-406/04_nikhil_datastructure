let Stack = require("./stacks");

let givenArray = [5, 1, 4, 9, 2, 5, 4, 3, 0];
let stack = new Stack();
let result = [];

for (let i = 0; i < givenArray.length; i++) {
  stack.push(givenArray[i]);
}

console.log(stack.displayStack());
let upperStack = new Stack();

while (stack.size > 0) {
  let currentNum = stack.pop();
  let tempStack = new Stack();
  tempStack = stack.clone();

  let lowerValue = 0;
  let upperValue = 0;

  while (tempStack.size) {
    let temp = tempStack.pop();
    if (temp < currentNum) {
      lowerValue = temp;
      break;
    }
    if (tempStack.size == 0) {
      lowerValue = 0;
      break;
    }
  }

  let tempUpperStack = new Stack();
  tempUpperStack = upperStack.clone();
  while (tempUpperStack.size > 0) {
    let temp = tempUpperStack.pop();
    if (temp < currentNum) {
      upperValue = temp;
      break;
    }
    if (tempUpperStack.size == 0) {
      upperValue = 0;
      break;
    }
  }
  upperStack.push(currentNum);

  let diff = Math.abs(lowerValue - upperValue);
  result.unshift(diff);
}

console.log(result);
