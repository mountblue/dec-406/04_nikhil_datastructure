let Stack = require("./stacks");
let result = [];
let givenArray = [2, 4, 6, 1, 5, 0];
let stack = new Stack();

for (let i = 0; i < givenArray.length; i++) {
  stack.push(givenArray[i]);
}

console.log(stack.displayStack());
while (stack.size > 1) {
  let currentValue = stack.pop();
  let tempStack = new Stack();
  tempStack = stack.clone();

  while (tempStack.size >= 1) {
    let presentValue = tempStack.pop();

    if (presentValue < currentValue) {
      result.unshift(presentValue);
      break;
    }

    if (tempStack.size == 0) {
      result.unshift(-1);
      break;
    }
  }
}
result.unshift(-1);

console.log(result);
